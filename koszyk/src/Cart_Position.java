
public class Cart_Position {

	private int quantity;
	private double totalPrice;
	private Product product = new Product();
	
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice() {
		this.totalPrice = getProduct().getPrice() * this.quantity;
	}
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}

	public Cart_Position(Product product, int quantity) {
		setProduct(product);
		setQuantity(quantity);
		setTotalPrice();		
	}
	
	public Cart_Position() {}
	
	public Cart_Position clone(){
		Cart_Position cl = new Cart_Position();
        cl.quantity = this.quantity;
        cl.totalPrice = this.totalPrice;
        cl.product = this.product;
        return cl;
    }
	
	@Override
	public String toString() {
		return product + ", ilo��: " + quantity + " szt., Cena ��czna: " + totalPrice + " z�,";
	}

}
