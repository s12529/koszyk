import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Cart {

	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Cart_Position product;
		List<Cart_Position> cart = new ArrayList<Cart_Position>();
		AnyForOneDiscount discount = new AnyForOneDiscount();
		Store wh = new Store();
		
		int option = -1;
		
		while(option!=0) {
		System.out.println(printInfo());
		option = sc.nextInt();	
		switch(option) {
			
			case 0: System.out.println("Koniec"); break;
			case 1: 
				
				System.out.println("Dzisiaj specjalna promocja na gazety i papierosy! Pi�� sztuk w cenie czterech!");
				wh.printStore();
				int choose = sc.nextInt();
				sc.nextLine();
	
				if(choose >= 0 && choose <= wh.magazine.size()) {
				choose--;
				System.out.println("Ile sztuk chcesz kupi�?");
				product = new Cart_Position(wh.magazine.get(choose),sc.nextInt());
				cart.add(product);
				} else System.out.println("B��d");

				break;
			case 2: 
				
				if(cart.size() == 0) {
					System.out.println("Koszyk jest pusty.");
				} else {
				System.out.println("Zawarto�� koszyka: ");	
				printCart(discount.discount(cart)); }
				break;
				
			case 3:
				
				if(cart.size() == 0) {
					System.out.println("Koszyk jest pusty.");
				} else {
				System.out.println("Zawarto�� koszyka: ");	
				printCart(cart); 
				
				break;}
			default: System.out.println("b��d");
		
			}

		}	
		sc.close();
	}

	
	
	private static String printInfo() {
		return "Witaj w naszym sklepie!\nMenu.\n0 - Wyjd�.\n1 - Dodaj produkt do koszyka.\n2 - Wy�wietl koszyk.\n3 - Wy�wietl koszyk (bez zni�ek).";
	}
	
	
	private static void printCart(List<Cart_Position> pCart) {
		int i = 0;
		double totalCartPrice = 0;
		
		for(Cart_Position tmp: pCart) {
			i++;
			System.out.println(i + ". " + tmp);
			totalCartPrice += tmp.getTotalPrice();
		}		
		System.out.println("��cznie: " + totalCartPrice + " z�.");
	}
}


