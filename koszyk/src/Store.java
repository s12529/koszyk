import java.util.ArrayList;
import java.util.List;

public class Store {

	List<Product> magazine;
	private Product product;
	
	public Store() {
		magazine = new ArrayList<Product>();
		product = new Product();
		
		product = new Product(1,"Gazeta",1.89);
		magazine.add(product);
		product = new Product(2,"D�ugopis",1.49);
		magazine.add(product);
		product = new Product(3,"Papierosy",12.00);
		magazine.add(product);
		product = new Product(4,"Grzebie�",4.99);
		magazine.add(product);
		product = new Product(5,"Proszek do prania",23.99);
		magazine.add(product);
		product = new Product(6,"Maskotka",14.79);
		magazine.add(product);		
	}

	 void printStore() {
		int i = 1;
		for(Product tmp:magazine) {
			System.out.println(i + ". " + tmp);
			i++;
		}
		
	}
	
	
}
