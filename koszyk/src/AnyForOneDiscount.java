import java.util.ArrayList;
import java.util.List;

public class AnyForOneDiscount implements Discount{
	private int any = 5;
	
	public List<Cart_Position> discount(List<Cart_Position> cartPosition) {
	
	List<Cart_Position> ccart = new ArrayList<Cart_Position>();

	
		for(Cart_Position tmp: cartPosition)
			{
			ccart.add(tmp.clone());
			}
		
		for(Cart_Position tmp: ccart)
			{
			if(tmp.getProduct().getProductId() == 1 || tmp.getProduct().getProductId() == 3) {
				if(tmp.getQuantity()>= any)
				{
					tmp.setQuantity(tmp.getQuantity()-(tmp.getQuantity()/any));	
					tmp.setTotalPrice();
				}
			}
		}
		return ccart;	
	}
		
	public void checkDiscount(Cart cart) {
		//????????
	}
	
	
}
