import java.util.List;

public interface Discount {

	List<Cart_Position> discount(List<Cart_Position> cartPosition);
	void checkDiscount(Cart cart);
}
